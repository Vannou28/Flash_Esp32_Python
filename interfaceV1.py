# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interface.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!

import serial.tools.list_ports
import subprocess
import sys

import threading


from notifypy import Notify
from PyQt5 import QtCore, QtGui, QtWidgets

class myThread (threading.Thread):
    def __init__(self, threadID, name, command):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.commande = command

    def run(self):
        print ('Starting ' + self.name)
        # Get lock to synchronize threads
        threadLock.acquire()
        print_commande(self.name, self.commande)
        # Free lock to release next thread
        threadLock.release()

def print_commande(threadName, commande):
   
    ui.bp_refresh.setEnabled(False)
    ui.bp_flash.setEnabled(False)
    
    procExe = subprocess.Popen(commande, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

    while procExe.poll() is None:
        line = procExe.stdout.readline()
        print("Print:" + threadName + ' ' + line)
        ui.listWidget.addItem(str(line))
        ui.listWidget.scrollToBottom()

    ui.listWidget.addItem(commande + ' : effectuée')
    notification = Notify()    
    notification.title = 'Processus ' + threadName
    notification.message = commande + ' : effectuée'
    notification.send()

    ui.bp_refresh.setEnabled(False)
    ui.bp_flash.setEnabled(False)



class Ui_view_select_port(object):
    def setupUi(self, view_select_port):
        view_select_port.setObjectName("view_select_port")
        view_select_port.resize(800, 400)
        self.groupBox = QtWidgets.QGroupBox(view_select_port)
        self.groupBox.setGeometry(QtCore.QRect(20, 10, 751, 381))
        self.groupBox.setObjectName("groupBox")
        self.bp_refresh = QtWidgets.QPushButton(self.groupBox)
        self.bp_refresh.setGeometry(QtCore.QRect(510, 70, 101, 25))
        self.bp_refresh.setObjectName("bp_refresh")
        self.listView_ports = QtWidgets.QListWidget(self.groupBox)
        self.listView_ports.setGeometry(QtCore.QRect(190, 60, 301, 91))
        self.listView_ports.setObjectName("listView_ports")
        self.lbl_ports = QtWidgets.QLabel(self.groupBox)
        self.lbl_ports.setGeometry(QtCore.QRect(10, 60, 161, 20))
        self.lbl_ports.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_ports.setStyleSheet("font: 15pt \"Ubuntu\";")
        self.lbl_ports.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_ports.setObjectName("lbl_ports")
        self.bp_flash = QtWidgets.QPushButton(self.groupBox)
        self.bp_flash.setGeometry(QtCore.QRect(510, 100, 101, 51))
        self.bp_flash.setObjectName("bp_flash")
        self.listWidget = QtWidgets.QListWidget(self.groupBox)
        self.listWidget.setGeometry(QtCore.QRect(190, 180, 421, 191))
        self.listWidget.setStyleSheet("font: 10pt \"Ubuntu\";")
        self.listWidget.setObjectName("listWidget")
        
        self.lbl_ports_2 = QtWidgets.QLabel(self.groupBox)
        self.lbl_ports_2.setGeometry(QtCore.QRect(10, 180, 101, 20))
        self.lbl_ports_2.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_ports_2.setStyleSheet("font: 15pt \"Ubuntu\";")
        self.lbl_ports_2.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_ports_2.setObjectName("lbl_ports_2")

        self.listWidget.setVisible(False)
        self.lbl_ports_2.setVisible(False)
        self.retranslateUi(view_select_port)
        QtCore.QMetaObject.connectSlotsByName(view_select_port)

    def retranslateUi(self, view_select_port):
        _translate = QtCore.QCoreApplication.translate
        view_select_port.setWindowTitle(_translate("view_select_port", "Select Port Com"))
        self.groupBox.setTitle(_translate("view_select_port", "Carte 1"))
        self.bp_refresh.setText(_translate("view_select_port", "Actualiser"))
        self.lbl_ports.setText(_translate("view_select_port", "Listes des ports"))
        self.bp_flash.setText(_translate("view_select_port", "Flash"))
        self.lbl_ports_2.setText(_translate("view_select_port", "Terminal"))
    


if __name__ == "__main__":

    def loadPort(card):
        ui.listWidget.setVisible(False)
        ui.lbl_ports_2.setVisible(False)

        comlist = serial.tools.list_ports.comports()
        connected = []
        for element in comlist:
            connected.append(element.device)

        print(card)
        ui.listView_ports.clear()
        ui.listView_ports.addItems(connected)

        return connected


    def portSelected(card):
        itemSelected = ui.listView_ports.currentItem();
        print(itemSelected.text())


    def commandSend(card):
    
        itemSelected = ui.listView_ports.currentItem();

        if(itemSelected==None):
            notification = Notify()
            notification.title = 'Ports Com'
            notification.message = 'Veuillez selectionner un port com'
            notification.send()
            return

 
        itemSelected = ui.listView_ports.currentItem().text();
        #print('esptool.py --port '+ str(itemSelected) +' erase_flash')
        #commande = 'esptool.py --port '+ str(itemSelected) +' erase_flash'

        print('esptool.py --port '+ str(itemSelected) +' erase_flash && esptool.py --chip esp32 --port '+ str(itemSelected) +' write_flash -z 0x1000 esp32-20210902-v1.17.bin')
        commande = 'esptool.py --port '+ str(itemSelected) +' erase_flash && esptool.py --chip esp32 --port '+ str(itemSelected) +' write_flash -z 0x1000 esp32-20210902-v1.17.bin'

        ui.listWidget.setVisible(True)
        ui.lbl_ports_2.setVisible(True)
        
        threads = []

        # Create new threads
        thread1 = myThread(1, "Commande-1", commande)
        #thread2 = myThread(2, "Thread-2", 2)

        # Start new Threads
        thread1.start()
        #thread2.start()

        # Add threads to thread list
        threads.append(thread1)
        #threads.append(thread2)

        # Wait for all threads to complete
        #for t in threads:
        #    t.join()

        

        #procExe = subprocess.Popen(commande , stdout=sys.stdout, stderr=sys.stdout)

        #while procExe.poll() is None:
        #    line = procExe.stdout.readline()
        #    print("Print:" + line)
         #   ui.textBrowser_serial.setText(str(line))
    

       
        
        #result = os.popen('esptool.py --port '+ str(itemSelected) +' erase_flash && esptool.py --chip esp32 --port '+ str(itemSelected) +' write_flash -z 0x1000 esp32-20210902-v1.17.bin').readlines()
        # print(result)
        #result = [x.rstrip() for x in result]
        #ui.textBrowser_serial.setText(str(result))
    



    
    app = QtWidgets.QApplication(sys.argv)
    view_select_port = QtWidgets.QDialog()
    ui = Ui_view_select_port()
    ui.setupUi(view_select_port)
    threadLock = threading.Lock()
    view_select_port.show()    
    loadPort(1)
    ui.bp_refresh.clicked.connect(lambda: loadPort(1))
    ui.bp_flash.clicked.connect(lambda:commandSend(1))   

    sys.exit(app.exec_())


