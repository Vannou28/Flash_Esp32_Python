# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interface.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_view_select_port(object):
    def setupUi(self, view_select_port):
        view_select_port.setObjectName("view_select_port")
        view_select_port.resize(800, 400)
        self.groupBox = QtWidgets.QGroupBox(view_select_port)
        self.groupBox.setGeometry(QtCore.QRect(20, 10, 751, 381))
        self.groupBox.setObjectName("groupBox")
        self.bp_refresh = QtWidgets.QPushButton(self.groupBox)
        self.bp_refresh.setGeometry(QtCore.QRect(510, 70, 101, 25))
        self.bp_refresh.setObjectName("bp_refresh")
        self.listView_ports = QtWidgets.QListWidget(self.groupBox)
        self.listView_ports.setGeometry(QtCore.QRect(190, 60, 301, 91))
        self.listView_ports.setObjectName("listView_ports")
        self.lbl_ports = QtWidgets.QLabel(self.groupBox)
        self.lbl_ports.setGeometry(QtCore.QRect(10, 60, 161, 20))
        self.lbl_ports.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_ports.setStyleSheet("font: 15pt \"Ubuntu\";")
        self.lbl_ports.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_ports.setObjectName("lbl_ports")
        self.bp_flash = QtWidgets.QPushButton(self.groupBox)
        self.bp_flash.setGeometry(QtCore.QRect(510, 100, 101, 51))
        self.bp_flash.setObjectName("bp_flash")
        self.listWidget = QtWidgets.QListWidget(self.groupBox)
        self.listWidget.setGeometry(QtCore.QRect(190, 180, 421, 191))
        self.listWidget.setStyleSheet("background-color: rgb(46, 52, 54);\n"
"color: rgb(78, 154, 6);\n"
"font: 8pt \"Ubuntu\";")
        self.listWidget.setObjectName("listWidget")
        self.lbl_ports_2 = QtWidgets.QLabel(self.groupBox)
        self.lbl_ports_2.setGeometry(QtCore.QRect(10, 180, 101, 20))
        self.lbl_ports_2.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_ports_2.setStyleSheet("font: 15pt \"Ubuntu\";")
        self.lbl_ports_2.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_ports_2.setObjectName("lbl_ports_2")

        self.retranslateUi(view_select_port)
        QtCore.QMetaObject.connectSlotsByName(view_select_port)

    def retranslateUi(self, view_select_port):
        _translate = QtCore.QCoreApplication.translate
        view_select_port.setWindowTitle(_translate("view_select_port", "Select Port Com"))
        self.groupBox.setTitle(_translate("view_select_port", "Carte 1"))
        self.bp_refresh.setText(_translate("view_select_port", "Actualiser"))
        self.lbl_ports.setText(_translate("view_select_port", "Listes des ports"))
        self.bp_flash.setText(_translate("view_select_port", "Flash"))
        self.lbl_ports_2.setText(_translate("view_select_port", "Terminal"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    view_select_port = QtWidgets.QDialog()
    ui = Ui_view_select_port()
    ui.setupUi(view_select_port)
    view_select_port.show()
    sys.exit(app.exec_())
